# Unity IAP

## Integration Steps

1) Import the **In App Purchasing** package from the Package Manager in Unity and follow the steps described in the <a href="https://docs.unity3d.com/Packages/com.unity.purchasing@4.5/manual/GettingStarted.html" target="_blank">Unity documentation</a>.

2) You will also need to generate Receipt validation classes by adding your app Base64-encoded RSA public key and following the steps in Unity toolbar **Services > In-App Purchasing > Receipt Validation Obfuscator** (step 4. is not mandatory).

![](_source/iap_ReceiptValidationObfuscator.png)

3) **"Install"** or **"Upload"** FG UnityIAP plugin from the FunGames Integration Manager in Unity, or download it from here.

4) Click on the **"Prefabs and Settings"** button from FunGames Integration window to fill up your scene with required components and create the Settings asset.
